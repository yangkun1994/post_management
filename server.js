// 1. 引入 express
const express = require('express')

require('express-async-errors')

// 2. 引入抽离出去的路由文件
const postRouter = require('./routers/postRouter')

// 3. 实例化express
const app = express()

app.use(express.json())
app.use(express.urlencoded({extend: true}))
// 4. 引用路由文件，并设置路由前缀
app.use('/posts', postRouter)

// 静态资源托管
app.use(express.static('./public'))

// 统一错误处理
app.use(function (err, req, res, next) {
    console.error(err.stack)
    res.status(500).send(err.message)
})

// 5. 监听接口，启动服务
app.listen(3000, () => {
    console.log('服务启动成功！')
})