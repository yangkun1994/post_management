# POST_MANAGEMENT

## MVC 架构模式 
1. M    model       数据层
1. V    views       视图层
1. C    controller  控制层

## 接口文档

### 使用步骤

1. 安装 apidoc
```bash
    npm install acidoc
```

apidoc 提供了一个 acidoc 命令，可以通过 apidoc -v 去验证

因为我们不是全局安装，所以要验证 apidoc 命令需要用一下三种方式
    -   cd node_modules/.bin    apidoc -v
    -   npx apidoc -v           npx 是 npm 5.X 版本之后提供的一个命令
    -   在 package.json 中使用命令  
2. 在每个路由代码前写上 acidoc 规定的信息
3. 在项目下创建一个 apidoc.json 文件，配置 api 接口文档的一些描述信息
4. 通过 acidoc 的命令去生成 api 接口文档

```bash
    - apidoc -i 写注释的路径 -o 文档的输出路径
    - apidoc -i ./routers -o ./public/docs 
```
4. 访问文档

5. 重构与改进
    1.期待能统一的去做错误处理, 在 server.js 中定义一个错误处理的中间件，放在最后
    app.use(function (err, req, res, next) {
        console.error(err.stack)
        res.status(500).send('Something broke!')
    })

    2. 为了能够处理 async await 产生的异常，还需要去使用一个依赖包 express-async-errors
    
