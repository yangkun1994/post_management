// 链接数据库 Mongodb

// 1. 引入 mongoose
const mongoose = require('mongoose')

// 2. 定义链接地址
const url = 'mongodb://localhost:27017/express'

// 3. 链接数据库
mongoose.connect(url).then(() => {
    console.log('数据库连接成功！')
}).catch((error) => {
    console.log('数据库连接失败！', error)
})

module.exports = mongoose