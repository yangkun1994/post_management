// post 路由文件

// 1. 引入 express
const express = require('express')

// 2. 生成 express Router 实例
const router = express.Router()

// 3. 引入 postController
const { getLists, create, update, remove } = require('../controllers/postController')

// 4. 定义帖子的路由（接口）
/**
 * @api {get} /posts 获取帖子列表
 * @apiName GetLists
 * @apiGroup GET 
 *
 * @apiSuccess {Number} code 错误状态吗.
 * @apiSuccess {String} msg 错误消息.
 * @apiSuccess {Array} data 帖子数组.
 */
router.get('/', getLists)

// POST /posts
/**
 * @api {post} /posts 新增帖子
 * @apiName create
 * @apiGroup POST 
 *
 * @apiParam {String} title 帖子标题.
 * @apiParam {String} content 帖子内容.

 * @apiSuccess {Number} code 错误状态吗.
 * @apiSuccess {String} msg 错误消息.
 * @apiSuccess {Array} data 帖子数组.
 */
router.post('/', create)

// PUT /posts/:id
/**
 * @api {put} /posts/:id 更新帖子
 * @apiName update
 * @apiGroup PUT 
 * 
 * @apiParam {String} ID 帖子id.
 * 
 * @apiSuccess {Number} code 错误状态吗.
 * @apiSuccess {String} msg 错误消息.
 * @apiSuccess {Array} data 帖子数组.
 */
router.put('/:id', update)

// DELETE /posts/:id
/**
 * @api {delete} /posts/:id 删除帖子
 * @apiName delete
 * @apiGroup DELETE 
 *
 * @apiParam {String} ID 帖子id.
 * 
 * @apiSuccess {Number} code 错误状态吗.
 * @apiSuccess {String} msg 错误消息.
 * @apiSuccess {Array} data 帖子数组.
 */
router.delete('/:id', remove)

module.exports = router