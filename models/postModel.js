// 帖子模型文件

// 引入已经连接到 Mongodb 的 mongoose
const mongoose = require('../config/db')

// 定义 schema
const postSchema = new mongoose.Schema({
    /**
     * 帖子标题
     *      type: 类型
     *      required: 必填项
     */
    title: {type: String, required: true},

    /**
     * 帖子内容
     *      type: Sting
     *      require: true
     */
    content: {type: String, required: true},
}, {
    /**
     * timestamps 为 true，会多出两个字段
     *      createAt:
     *      updateAt:
     */
    timestamps: true
})

// 创建模型
const PostModel = mongoose.model('post', postSchema)

// 暴露模型
module.exports = PostModel;