const PostModel = require('../models/postModel')
// 帖子的控制器，暴露一些列中间件方法，给到帖子的路由去使用

/**
 * 获取帖子列表
 * @param {*} req 
 * @param {*} res 
 */
exports.getLists = async (req, res) => {
    const data = await PostModel.find()
    res.send({ code: 0, msg: data})    
}

/**
 * 新增帖子
 * @param {*} req 
 * @param {*} res 
 */
exports.create = async (req, res) => {
    const { title, content } = req.body;
    await PostModel.create({title, content})
    res.send({ code: 0, msg: 'OK' })
}

/**
 * 更新帖子
 * @param {*} req 
 * @param {*} res 
 */
exports. update = async (req, res) => {
    // 1. 要更新的帖子的 ID
    const { id } = req.params

    // 2. 更新的内容
    const { title, content } = req.body

    // 3. Model.updateOne
    await PostModel.updateOne({_id: id}, { title, content })
    res.send({ code: 0, msg: 'OK' })
}


/**
 * 删除帖子
 * @param {*} req 
 * @param {*} res 
 */
exports.remove = async (req, res) => {
    const { id } = req.params
    await PostModel.deleteOne({_id: id})
    res.send({ code: 0, msg: 'OK' })
}
